
const fs = require('fs');
const _ = require('lodash')
class baseHelper {
    constructor() {
    }
    async getPathFile(req, res) {
        let dir = 'import';
        let files = req.files;
        let fileUpload = req.files.file;
        let name = fileUpload.name.split(".");
        let typeFile = name[name.length - 1];

        let date = new Date()
        let year = date.getFullYear();
        let month = date.getMonth() + 1;

        let now = _.now();

        if (!files) {
            return this.responseError(res, 400, "import_file_not_found");
        }

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }

        if (!fs.existsSync(`${dir}/${year}`)) {
            fs.mkdirSync(`${dir}/${year}`);
        }

        if (!fs.existsSync(`${dir}/${year}/${month}`)) {
            fs.mkdirSync(`${dir}/${year}/${month}`);
        }
        let pathFile = `${dir}/${year}/${month}/${now}.${typeFile}`;
        let upLoad = await fileUpload.mv(pathFile);
        if (!upLoad) {
            return pathFile;
        }

    }

    responseOk(res, message) {
        res.status(200).send({
            data: message
        })
    }

    responseError(res, message) {
        res.status(500).send({
            message: (message) ? message : "error"
        })
    }


    responseWarning(res, message) {
        res.status(400).send({
            message: (message) ? message : "error"
        })
    }

    /**
     * 
     * @param {String} date 
     */
    convertToDateFormat(date) {
        if (typeof date === "object")
            return this.convertDateOjectToTrueDateFormat(date);
        if (typeof date === "string")
            return this.convertStringToDateFormatBySymol(date, "-");

    }


    /**
     * @param  {Date} objDate
     */
    convertDateOjectToTrueDateFormat(objDate) {
        let year = objDate.getFullYear();
        // vì dữ liệu trong file excel xuất ra đang bị ngược
        let month = (objDate.getDate() < 10) ? `0${objDate.getDate()}` : objDate.getDate();
        let date = (objDate.getMonth() + 1 < 10) ? `0${objDate.getMonth() + 1}` : objDate.getMonth() + 1;
        return new Date(`${year}-${month}-${date}`)

    }

    /**
     * 
     * @param {String} stringDate 
     */
    convertStringToDateFormatBySymol(stringDate, symbol) {
        let arr_date = stringDate.split(symbol);
        return new Date(`${arr_date[2]}-${arr_date[0]}-${arr_date[1]}`);

    };



    getDateStartAndDateEndofMonth(month, year) {
        month = (parseInt(month) < 10) ? `0${month}` : month;

        let fromDate = new Date(`${year}-${month}-01`),
            endDateOfMonth = new Date(year, fromDate.getMonth() + 1, 0),
            toDate = new Date(`${year}-${month}-${endDateOfMonth.getDate()}`);

        return { "startDate": fromDate, "endDate": toDate }
    }

    /**
     * 
     * @param {Array<Number>} values 
     */
    medianBalance(values) {
        if (values.length === 0) return 0;

        values.sort(function (a, b) {
            return a - b;
        });

        var half = Math.floor(values.length / 2);

        if (values.length % 2)
            return values[half];

        let result = (values[half - 1] + values[half]) / 2.0;
        result = Math.round(result, 2);

        return result;
    }

    calculateDateInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }

};


module.exports = baseHelper;


const Helper = require('./helper');
const base_Helpser = new Helper();
const Excel = require('exceljs');
const satementModel = require('./model/statementebyday.model');
const { arr_median } = require('./config/config');
const goalModel = require('./model/goal.model');
class BaseController extends Helper {
    constructor() {
        super()
    };

    async readFileData(req, res) {
        try {
            let accountID = req.body.accountID;
            let pathfile = await base_Helpser.getPathFile(req, res);
            let workbook = new Excel.Workbook();
            let wb = await workbook.xlsx.readFile(pathfile);
            let worksheet = wb.getWorksheet(2);
            let arr_data_statement = [];
            worksheet.eachRow((row, rowNumber) => {
                if (rowNumber > 9) {
                    let data_statement = {
                        accountID: accountID,
                        date: (row.getCell(1).value) ? this.convertToDateFormat(row.getCell(1).value) : "",
                        number_of_date: this.convertToDateFormat(row.getCell(1).value).getDate(),
                        note: (row.getCell(2).value) ? row.getCell(2).value : "",
                        deposit: (row.getCell(3).value) ? row.getCell(3).value : 0,
                        withdraw: (row.getCell(4).value) ? row.getCell(4).value : 0,
                        balance: (row.getCell(5).value) ? row.getCell(5).value : 0,
                    }
                    // let idx = arr_data_statement.findIndex(i => i.number_of_date === data_statement.number_of_date);
                    // if (idx != -1)
                    //     arr_data_statement[idx] = data_statement;
                    // else
                    arr_data_statement.push(data_statement);
                }
            });

            let { startDate, endDate } = this.getNumberStartDateAndEndDateOfMonth(arr_data_statement[0].date);

            const arr_data_statement_lenght = arr_data_statement.length;

            for (let i = startDate; i <= endDate; i++) {
                for (let j = 0; j < arr_data_statement.length; j++) {
                    let oldDate = arr_data_statement[j].date,
                        year = oldDate.getFullYear(),
                        month = (oldDate.getMonth() + 1 < 10) ? `0${oldDate.getMonth() + 1}` : oldDate.getMonth() + 1,
                        date = (i < 10) ? `0${i}` : i,
                        idx = arr_data_statement.findIndex(m => m.number_of_date === i);

                    if (arr_data_statement[j].number_of_date > i && idx === -1) {
                        let new_obj = {
                            number_of_date: i,
                            accountID: accountID,
                            date: new Date(`${year}-${month}-${date}`),
                            balance: arr_data_statement[(j - 1) > 0 ? j - 1 : j].balance
                        };
                        arr_data_statement.push(new_obj);
                    }
                    if (arr_data_statement.length < i && idx === -1) {
                        let new_obj = {
                            number_of_date: i,
                            accountID: accountID,
                            date: new Date(`${year}-${month}-${date}`),
                            balance: arr_data_statement[arr_data_statement_lenght - 1].balance
                        };
                        arr_data_statement.push(new_obj);
                    }
                }
            };

            for (let item of arr_data_statement) {
                await satementModel.findOneAndUpdate({ "date": item.date, "accountID": accountID }, item, { upsert: true });
            }
            this.responseOk(res, "ok");
        } catch (error) {
            this.responseError(res, "error")
        }


    }

    getNumberStartDateAndEndDateOfMonth(date) {
        let month = date.getMonth() + 1,
            year = date.getFullYear();
        let data_Date = this.getDateStartAndDateEndofMonth(month, year);
        return { startDate: data_Date.startDate.getDate(), endDate: data_Date.endDate.getDate() };
    }


    async calculateMedian(req, res, accountID) {
        try {
            let median_balance = 0;
            let dateNow = new Date();
            // lấy ds số trung bình của 12 tháng
            let yearNow = dateNow.getFullYear() - 1,
                month = dateNow.getMonth(),
                previousYear = new Date(yearNow, month, 1);
            let arr_medium_balance_every_month = [];

            for (let i = 0; i <= 12; i++) {
                let year = new Date(previousYear);
                let result = 0;
                year.setMonth(month + i);
                let month_query = (year.getMonth() === 11) ? 12 : year.getMonth(),
                    year_query = year.getFullYear();

                if (month_query != 0) {
                    result = await this.getMediumNumberByMonth(month_query, year_query, accountID);
                    arr_medium_balance_every_month.push(result);
                }
                median_balance = this.medianBalance(arr_medium_balance_every_month);
            }
            median_balance = (median_balance) ? median_balance : 0;
            return median_balance;

        } catch (error) {
            console.log(error)
            this.responseError(res, "error")
        }

        // tính số trung vị dựa trên 12 tháng
    }

    async getMedianBalanceNumber(req, res) {
        try {
            let result = 0;
            let accountID = req.query.accountID;
            result = await this.calculateMedian(req, res, accountID);
            console.log(result)

            this.responseOk(res, result)
        } catch (error) {
            this.responseError(res, "error")
        }

    }

    async chartMedianModel(req, res) {
        try {
            let accountID = req.query.accountID;
            let median_balance = await this.calculateMedian(req, res, accountID);
            let query = [
                {
                    $match: { "accountID": accountID }
                },
                {
                    $project: {
                        month: { $month: "$date" },
                        year: { $year: "$date" },
                        balance: 1
                    }
                },
                {
                    $group: { _id: { month: "$month", year: "$year" } }
                }
            ];
            let obj = await satementModel.aggregate(query);

            let ar_median_by_month = JSON.parse(JSON.stringify(arr_median));

            for (let item of obj) {
                let month_query = item._id.month;
                let year_query = item._id.year;
                let medium_by_month_year = await this.getMediumNumberByMonth(month_query, year_query, accountID);
                if (medium_by_month_year > median_balance) {

                    let idx = ar_median_by_month.findIndex(item => item.month === parseInt(month_query));
                    if (idx != -1) {
                        ar_median_by_month[idx].median++;
                    }
                }

            }
            let result_median = ar_median_by_month.map(i => i.median);
            this.responseOk(res, result_median);
        } catch (error) {
            console.log(error)
            this.responseError(res, "error")
        }
    }
    /**
     * 
     * @param {Number} month 
     * @param {Number} year 
     */
    async getMediumNumberByMonth(month_query, year_query, accountID) {
        let result = 0;
        let numberDayofMonth = this.calculateDateInMonth(month_query, year_query);
        let query = [
            {
                $match: { "accountID": `${accountID}` }
            },
            {
                $project: {
                    month: { $month: "$date" },
                    year: { $year: "$date" },
                    balance: 1
                }

            },
            {
                $match: {
                    "month": month_query,
                    "year": year_query
                }
            },
            {
                $group: {
                    _id: null,
                    sumBalance: { $sum: "$balance" }
                }
            }
        ]

        let balance_data = await satementModel.aggregate(query);
        let sum_by_month = (balance_data[0]) ? balance_data[0].sumBalance : 0;
        result = (sum_by_month / numberDayofMonth);
        return result;
    }


    async getlistBalanceEveryMonthByYear(req, res) {
        try {
            let accountID = req.query.accountID;
            let median_balance = await this.calculateMedian(req, res, accountID);

            let resut = [];
            let year = req.query.year;
            if (!year)
                this.responseWarning(res, "vui long nhap so nam muon xem");

            for (let i = 1; i <= 12; i++) {
                let medium_blance = await this.getMediumNumberByMonth(i, parseInt(req.query.year), accountID);
                medium_blance = Math.round(medium_blance,2);               
                resut.push(medium_blance);
            }
            this.responseOk(res, {
                median_balance: median_balance,
                list_month: resut
            });
        } catch (error) {
            console.log(error)
            this.responseError(res, "error")
        }

    }



    async getListEveryDateByMonthAndYear(req, res) {
        try {
            let month = req.query.month,
                year = req.query.year,
                accountID = req.query.accountID,
                result = [];
            let { startDate, endDate } = this.getDateStartAndDateEndofMonth(month, year);
            let data = await satementModel.find({ accountID: accountID, date: { $gte: startDate, $lte: endDate } });
            result = data.map(i => i.balance);
            this.responseOk(res, result);
        } catch (error) {
            console.log(error)
            this.responseError(res, "error")
        }

    }


    async CreateGoal(req, res) {
        try {
            let data = req.body;
            if (!data.goalID) return this.responseWarning(res, "goalID không được rỗng!");
            if (!data.description) return this.responseWarning(res, "Mô tả không được rỗng!");
            if (typeof data.money != "number") this.responseWarning(res, "Số tiền nhập vào phải là số!");
            if (data.money === 0) this.responseWarning(res, "Số tiền nhập phải lớn hơn 0!");
            if (typeof data.month != "number") this.responseWarning(res, "Tháng nhập vào phải là số!");
            if (data.month < 1 || data.month > 12 || data.no) this.responseWarning(res, "Số tháng nhập phải nằm trong khoảng từ 1 đến 12 tháng!");

            data.money = parseInt(data.money);
            data.month = parseInt(data.month);
            let result = await goalModel.create(data);

            this.responseOk(res, result)
        } catch (error) {
            console.log(error)
            this.responseError(res, "error")
        }
    }

    async getListGoal(req, res) {
        try {
            let result = await goalModel.find();
            this.responseOk(res, result)
        } catch (error) {
            this.responseError(res, "error")
        }

    }

    async login(req, res) {
        let userID = req.body.userID,
            passWord = req.body.passWord;

    }
}




module.exports = BaseController;

const mongoose = require('mongoose');

const SatementByDayShemas = new mongoose.Schema({
    accountID: { type: String, default: "" },
    date: { type: Date },
    note: { type: String, default: "" },
    deposit: { type: Number, default: 0 },
    withdraw: { type: Number, default: 0 },
    balance: { type: Number, default: 0 }
}, { timestamps: true })


module.exports = mongoose.model('statementebyday', SatementByDayShemas);
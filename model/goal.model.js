//muctieu
//description
//so tien
//so thang :number

const mongoose = require('mongoose');
const goalSchemas = new mongoose.Schema({
    goalID: { type: String, default: "" },
    description: { type: String, default: "" },
    money: { type: Number, default: 0 },
    month: { type: Number, default: 0 }
});

module.exports = mongoose.model('goal', goalSchemas);

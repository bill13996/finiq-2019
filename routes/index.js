var express = require('express');
var router = express.Router();
const baseController = require('../controller')
const base_Controller = new baseController();
/* GET home page. */
// router.get('/', function (req, res) {
//   res.sendfile('index');
// });

router.post('/import-excel-data', (req, res) => {
  base_Controller.readFileData(req, res)
});



router.get('/get-data-median-for-chart', (req, res) => {
  base_Controller.chartMedianModel(req, res)
});

router.get('/get-median-balance-number', (req, res) => {
  base_Controller.getMedianBalanceNumber(req, res)
});

router.get('/get-medium-balance-every-month-by-year', (req, res) => {
  base_Controller.getlistBalanceEveryMonthByYear(req, res)
});

router.get('/get-medium-balance-every-date-by-month-and-year', (req, res) => {
  base_Controller.getListEveryDateByMonthAndYear(req, res)
});

router.post('/create-goal', (req, res) => {
  base_Controller.CreateGoal(req, res)
});


router.get('/get-list-goal', (req, res) => {
  base_Controller.getListGoal(req, res)
});










module.exports = router;

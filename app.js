var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fileupload = require("express-fileupload");

var routes = require('./routes/index');
var users = require('./routes/users');
var app = express();

const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
mongoose.Promise = global.Promise;
mongoose.plugin(mongoosePaginate);
const { url } = require('./config/config')
mongoose.set("toJSON", {
    virtuals: true
});
mongoose.set("toObject", {
    virtuals: true
});
// Connecting to the database
mongoose
    .connect(url, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Successfully connected to the database");
    })
    .catch(err => {
        console.log("Could not connect to the database. Exiting now...", err);
        process.exit();
    });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(fileupload());
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

/// catch 404 and forwarding to error handler
app.use('/static', express.static(path.join(__dirname, 'views/finiq/build/static')));
// app.get('*',function(req,res) {
//     res.sendfile('views/finiq/build/index.html');
//   });
app.use((req, res) => {

    res.sendfile(path.join(__dirname, 'views/finiq/build/index.html'));
  });
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers


// app.use((req, res) => {
//     res.sendFile(path.join(__dirname, 'views/finiq/build/index.html'));
// });



app.listen(5000, () => console.log("server running 5000"));


module.exports = app;

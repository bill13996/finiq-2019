import React, { Component } from "react";
import { Card, CardBody, Button, Row, Col, Progress } from "reactstrap";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import SANPHAM from "assets/img/icon-muctieu-2.svg";
import { connect } from "react-redux";
import { actGetDreamRequest } from "../../actions/dream.action";
import { formatMoney } from "utils/formatMoney";

class MucTieuCuaBan extends Component {
  state = {};

  componentDidMount() {
    this.props.getDream();
  }

  renderGoalID = id => {
    const muctieuList = [
      { label: "Mua nhà", value: "mua-nha" },
      { label: "Mua xe", value: "mua-xe" },
      { label: "Du lịch", value: "du-lich" },
      { label: "Tiết kiệm tháng", value: "tiet-kiem-thang" },
      { label: "Kết hôn", value: "ket-hon" },
      { label: "Mục tiêu khác", value: "muc-tieu-khac" }
    ];
    console.log(id);
    const result = muctieuList.find(prop => prop.value === id);
    return result ? result.label : "";
  };

  render() {
    const props = this.props;
    return (
      <>
        <Card className="card-muc-tieu-cua-ban">
          <CardBody>
            <Row>
              <Col lg="6 d-flex align-items-center">
                <div className="label">Mục tiêu của bạn</div>
              </Col>
              <Col lg="6 text-right">
                <Link to="/thiet-lap-muc-tieu">
                  <Button
                    style={{ backgroundColor: "#fca800", color: "white" }}
                  >
                    Tạo mục tiêu
                  </Button>
                </Link>
              </Col>
            </Row>
            <Row>
              {this.props.dreams.map((prop, key) => {
                return (
                  <div className="muctieu-item" key={key}>
                    <div className="muctieu">
                      <div
                        className="muctieu-image"
                        style={{ backgroundImage: `url(${SANPHAM})` }}
                      ></div>
                      <div className="muctieu-detail">
                        <div className="heading">
                          {this.renderGoalID(prop.goalID)}
                        </div>
                        <div className="description">{prop.description}</div>
                        <div className="money-time">
                          <div className="money mr-3">
                            Tiền:
                            <span>{formatMoney(prop.money) + " VND"}</span>
                          </div>
                          <div className="time">
                            Thời gian:
                            <span>{prop.month} tháng</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="muctieu-progress">
                      <div className="percentage">0%</div>
                      <div className="progress-container">
                        <Progress value="0" color="#009fe3" />
                      </div>
                    </div>
                  </div>
                );
              })}
            </Row>
          </CardBody>
        </Card>
      </>
    );
  }
}
MucTieuCuaBan.propTypes = {
  content: PropTypes.string
};

const mapStateToProps = state => {
  return {
    dreams: state.accountReducer.dreams
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDream: () => {
      dispatch(actGetDreamRequest());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MucTieuCuaBan);

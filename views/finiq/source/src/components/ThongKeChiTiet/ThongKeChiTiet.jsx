import React, { Component } from "react";
import {
  Card,
  CardBody,
  Row,
  Col,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "reactstrap";
import ReactDatetime from "react-datetime";
import { Line } from "react-chartjs-2";
import PropTypes from "prop-types";
import shuffleArray from "utils/shuffleArray";
import {
  products_tk_up,
  products_car_up,
  products_home_up,
  products_down
} from "../../variables/data";
import { connect } from "react-redux";
import {
  actGetChartMoneyByYearRequest,
  actGetStatMonthRequest
} from "../../actions/account.action";
import moment from "moment";

class ThongKeChiTiet extends Component {
  state = {
    isImported: true,
    chartType: "year",
    isRadienUp: true,
    time: moment()
  };

  componentDidMount() {
    this.getChartMoneyByYear();
  }

  handleOnChangeChartType(chartType) {
    this.setState({ chartType });
  }

  getChartMoneyByYear = () => {
    const year = this.state.time.year();
    this.props.getChartMoneyByYear(year);
    this.props.getStatMonth(year);
  };

  renderProductsUp = () => {
    return (
      <div className="d-flex flex-wrap">
        {shuffleArray(products_tk_up).map((product, key) => {
          if (key < 2)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
        {shuffleArray(products_car_up).map((product, key) => {
          return (
            <div
              className="sanpham cursor-pointer"
              onClick={() => this.handleOnProductClick(product.url)}
              key={key}
            >
              <div
                className="sanpham-image"
                style={{ backgroundImage: `url(${product.image})` }}
              ></div>
              <div className="sanpham-detail">
                <div className="heading">{product.title}</div>
                <div className="description">{product.description}</div>
              </div>
            </div>
          );
        })}
        {shuffleArray(products_home_up).map((product, key) => {
          if (key < 1)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
      </div>
    );
  };

  renderProductsDown = () => {
    return (
      <div className="d-flex flex-wrap">
        {shuffleArray(products_down).map((product, key) => {
          if (key < 2)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
      </div>
    );
  };

  handleOnProductClick = url => {
    window.open(url, "_blank");
  };

  _handleOnChange(name, value) {
    const chartType = this.state.chartType;
    console.log(value.year());
    if (name === "time" && chartType === "year") {
      this.props.getChartMoneyByYear(value.year());
      this.props.getStatMonth(value.year());
    }
    if (name === "time" && chartType === "month") {
    }
    this.setState({
      [name]: value
    });
  }

  render() {
    const chart_money = this.props.chart_money;
    return (
      <>
        <Card className="card-hoat-dong-chi-tieu animated fadeIn">
          <CardBody>
            <div className="chart-tu-van mb-5">
              <Row>
                <Col lg="3 d-flex align-items-center">
                  <div className="label">Thống kê chi tiết</div>
                </Col>
                <Col lg="5 d-flex align-items-center">
                  <div className="d-inline-block">
                    <span className="color-green"></span>
                    Mức ổn định
                  </div>
                  <div className="d-inline-block">
                    <span className="color-blue-1"></span>
                    Số dư/Trung bình tháng
                  </div>
                </Col>
                <Col lg="4 d-flex align-items-center">
                  <FormGroup style={{ width: "100%" }}>
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-calendar-grid-58" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <ReactDatetime
                        inputProps={{
                          placeholder: "Year"
                        }}
                        dateFormat={"YYYY"}
                        value={this.state.time}
                        timeFormat={false}
                        onChange={value => this._handleOnChange("time", value)}
                        closeOnSelect={true}
                      />
                    </InputGroup>
                  </FormGroup>
                </Col>
              </Row>
              <div className="chart mt-4 mb-5">
                <Line
                  data={chart_money.data}
                  options={chart_money.options}
                  className="chart-canvas"
                  height={100}
                  id="chart-line"
                />
              </div>
            </div>
            <div className="san-pham-goi-y mt-5">
              <div className="label">Có thể bạn quan tâm</div>
              {this.state.isRadienUp
                ? this.renderProductsUp()
                : this.renderProductsDown()}
            </div>
          </CardBody>
        </Card>
      </>
    );
  }
}
ThongKeChiTiet.propTypes = {
  content: PropTypes.string
};

const mapStateToProps = state => {
  return {
    chart_money: state.accountReducer.chart_money,
    top3BestMoneyMonth: state.accountReducer.top3BestMoneyMonth,
    top3WorstMoneyMonth: state.accountReducer.top3WorstMoneyMonth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getChartMoneyByYear: year => {
      dispatch(actGetChartMoneyByYearRequest(year));
    },
    getStatMonth: year => {
      dispatch(actGetStatMonthRequest(year));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThongKeChiTiet);

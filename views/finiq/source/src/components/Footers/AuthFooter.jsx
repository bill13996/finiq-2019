/*eslint-disable*/
import React from "react";

// reactstrap components
import { NavItem, NavLink, Nav, Container, Row, Col } from "reactstrap";

class Login extends React.Component {
  render() {
    return (
      <>
        <footer className="" id="footer-main" style={{backgroundColor: "#0E2D95", marginTop: "-70px"}}>
          <Container>
            <Row className="align-items-center justify-content-xl-between text-center">
              <Col xl="12">
                <div className="copyright text-center text-muted">
                  © {new Date().getFullYear()}{" "}
                  <a
                    className="font-weight-bold ml-1"
                    href={window.location.href}
                    target="_blank"
                  >
                    FINIQ
                  </a>
                </div>
              </Col>
            </Row>
          </Container>
        </footer>
      </>
    );
  }
}

export default Login;

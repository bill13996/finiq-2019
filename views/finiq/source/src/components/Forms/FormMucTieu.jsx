import React, { Component } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  FormGroup,
  Button,
  Input,
  Label
} from "reactstrap";
import PropTypes from "prop-types";
import Select from "react-select";
import TuVanModal from "../Modals/TuVanModal";

const muctieuList = [
  { label: "Mua nhà", value: "mua-nha" },
  { label: "Mua xe", value: "mua-xe" },
  { label: "Du lịch", value: "du-lich" },
  { label: "Tiết kiệm tháng", value: "tiet-kiem-thang" },
  { label: "Kết hôn", value: "ket-hon" },
  { label: "Mục tiêu khác", value: "muc-tieu-khac" }
];

class FormMucTieu extends Component {
  state = {
    isOpen: false,
    goal_money: 0,
    current_balance: 0,
    monthly_time: 0,
    monthly_money: 0
  };

  handleOnSubmit = () => {
    this.toogleModal();
    this.setState({
      monthly_money:
        (this.state.goal_money - this.state.current_balance) /
        this.state.monthly_time
    });
  };

  _handleOnChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  toogleModal = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  render() {
    const props = this.props;
    return (
      <>
        <TuVanModal
          isOpen={this.state.isOpen}
          toggle={this.toogleModal}
          data={this.state}
          muctieuSelect={this.state.muctieuSelect}
          goal_desc={this.state.goal_desc}
          current_balance={this.state.current_balance}
          goal_money={this.state.goal_money}
          monthly_time={this.state.monthly_time}
          monthly_money={this.state.monthly_money}
          className="mt-5"
        />
        <Card className="form-muc-tieu">
          <CardHeader>
            <div className="label">
              CHÀO {localStorage.getItem("adminName")}, <br /> MỤC TIÊU CỦA BẠN
              LÀ GÌ?
            </div>
          </CardHeader>
          <CardBody>
            <Row>
              <Col lg="6">
                <FormGroup>
                  <Label>Mục tiêu của bạn là gì</Label>
                  <Select
                    className="react-select"
                    classNamePrefix="react-select"
                    placeholder="Mục tiêu của bạn là gì"
                    size="lg"
                    name="muctieu"
                    value={this.state.muctieuSelect}
                    options={muctieuList}
                    onChange={value =>
                      this._handleOnChange("muctieuSelect", value)
                    }
                  />
                </FormGroup>
              </Col>
              <Col lg="6">
                <FormGroup>
                  <Label>Miêu tả mục tiêu (Ex: Mua nhà Vinhome)</Label>
                  <Input
                    type="text"
                    value={this.state.goal_desc}
                    onChange={e =>
                      this._handleOnChange("goal_desc", e.target.value)
                    }
                    placeholder="Miêu tả mục tiêu (Ex: Mua nhà Vinhome)"
                  />
                </FormGroup>
              </Col>
              <Col lg="6">
                <FormGroup>
                  <Label>Số tiền của mục tiêu</Label>
                  <Input
                    type="number"
                    value={this.state.goal_money}
                    onChange={e =>
                      this._handleOnChange("goal_money", e.target.value)
                    }
                    placeholder="Số tiền của mục tiêu"
                  />
                </FormGroup>
              </Col>
              <Col lg="6">
                <FormGroup>
                  <Label>Số tiền bạn hiện có</Label>
                  <Input
                    type="number"
                    value={this.state.current_balance}
                    onChange={e =>
                      this._handleOnChange("current_balance", e.target.value)
                    }
                    placeholder="Số tiền bạn hiện có"
                  />
                </FormGroup>
              </Col>
              <Col lg="6">
                <FormGroup>
                  <Label>Số tháng bạn muốn đạt mục tiêu</Label>
                  <Input
                    type="number"
                    value={this.state.monthly_time}
                    onChange={e =>
                      this._handleOnChange("monthly_time", e.target.value)
                    }
                    placeholder="Số tháng bạn muốn đạt mục tiêu"
                  />
                </FormGroup>
              </Col>
              <Col lg="6 text-right">
                <Button
                  disabled={
                    !this.state.muctieuSelect ||
                    !this.state.goal_desc ||
                    !this.state.goal_money ||
                    !this.state.current_balance ||
                    !this.state.monthly_time
                  }
                  onClick={this.handleOnSubmit}
                >
                  Tư vấn
                </Button>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </>
    );
  }
}
FormMucTieu.propTypes = {
  content: PropTypes.string
};

export default FormMucTieu;

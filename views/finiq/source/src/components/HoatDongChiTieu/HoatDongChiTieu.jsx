import React, { Component } from "react";
import { Card, CardBody, Row, Col } from "reactstrap";
import { Line } from "react-chartjs-2";
import { chartTinhDiem } from "variables/charts.jsx";
import { Link } from "react-router-dom";
import shuffleArray from "utils/shuffleArray";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { actGetProductRequest } from "../../actions/product.action";
import {
  products_tk_up,
  products_car_up,
  products_home_up,
  products_down
} from "../../variables/data";
import {
  actGetChartPointRequest,
  actGetChartMoneyByYearRequest
} from "../../actions/account.action";
import moment from "moment";

class HoatDongChiTieu extends Component {
  state = {
    isImported: true,
    isRadienUp: true
  };

  componentDidMount() {
    this.getProducts();
    this.getChartPoint();
  }

  getProducts = () => {
    this.props.getProducts({
      product_class: "CORE",
      product_group: "TD"
    });
  };

  getChartPoint = () => {
    this.props.getChartPoint();
  };

  renderProductsUp = () => {
    return (
      <div className="d-flex flex-wrap">
        {shuffleArray(products_tk_up).map((product, key) => {
          if (key < 2)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
        {shuffleArray(products_car_up).map((product, key) => {
          return (
            <div
              className="sanpham cursor-pointer"
              onClick={() => this.handleOnProductClick(product.url)}
              key={key}
            >
              <div
                className="sanpham-image"
                style={{ backgroundImage: `url(${product.image})` }}
              ></div>
              <div className="sanpham-detail">
                <div className="heading">{product.title}</div>
                <div className="description">{product.description}</div>
              </div>
            </div>
          );
        })}
        {shuffleArray(products_home_up).map((product, key) => {
          if (key < 1)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
      </div>
    );
  };

  renderProductsDown = () => {
    return (
      <div className="d-flex flex-wrap">
        {shuffleArray(products_down).map((product, key) => {
          if (key < 2)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
      </div>
    );
  };

  handleOnProductClick = url => {
    window.open(url, "_blank");
  };

  render() {
    const chart_point = this.props.chart_point;
    return (
      <>
        <Card className="card-hoat-dong-chi-tieu animated fadeIn">
          <CardBody>
            <div className="chart-tu-van mb-5">
              <Row>
                <Col lg="3">
                  <div className="label">Hoạt động chi tiêu</div>
                </Col>
                <Col lg="6">
                  <div className="d-inline-block">
                    <span className="color-yellow"></span>
                    Mức ổn định
                  </div>
                  <div className="d-inline-block">
                    <span className="color-blue"></span>
                    Khả năng kiểm soát chi tiêu
                  </div>
                </Col>
                <Col lg="3 text-right">
                  <Link to="/thong-ke-chi-tiet" className="link-detail">
                    Xem chi tiết
                  </Link>
                </Col>
              </Row>
              <div className="chart mt-4 mb-5">
                <Line
                  data={chart_point.data}
                  options={chart_point.options}
                  className="chart-canvas"
                  height={100}
                  id="chart-line"
                />
              </div>
            </div>
            <div className="tu-van-thang mt-5">
              <Row>
                <div className="column">
                  <div className="heading">Tiết kiệm nhiều hơn:</div>
                  <Row>
                    <Col lg="3">Tháng 2</Col>
                    <Col lg="3">Tháng 4</Col>
                    <Col lg="3">Tháng 8</Col>
                  </Row>
                </div>
                <div className="column pl-4">
                  <div className="heading">Chú ý chi tiêu:</div>
                  <Row>
                    <Col lg="3">Tháng 6</Col>
                    <Col lg="3">Tháng 7</Col>
                    <Col lg="3">Tháng 9</Col>
                  </Row>
                </div>
                <div className="column pl-4">
                  <div className="heading">Tháng hiện tại:</div>
                  <Row>
                    <Col lg="12">Lưu ý chi tiêu</Col>
                  </Row>
                </div>
                <div className="pl-4">
                  <div className="heading">Tháng tiếp theo:</div>
                  <Row>
                    <Col lg="12">Lưu ý chi tiêu</Col>
                  </Row>
                </div>
              </Row>
            </div>
            <div className="san-pham-goi-y mt-5">
              <div className="label">Có thể bạn quan tâm</div>
              {this.state.isRadienUp
                ? this.renderProductsUp()
                : this.renderProductsDown()}
            </div>
          </CardBody>
        </Card>
      </>
    );
  }
}
HoatDongChiTieu.propTypes = {
  content: PropTypes.string
};

const mapStateToProps = state => {
  return {
    products: state.productReducer.products,
    chart_point: state.accountReducer.chart_point
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProducts: data => {
      dispatch(actGetProductRequest(data));
    },
    getChartPoint: () => {
      dispatch(actGetChartPointRequest());
    },
    getChartMoneyByYear: year => {
      dispatch(actGetChartMoneyByYearRequest(year));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HoatDongChiTieu);

HoatDongChiTieu.propTypes = {
  getProducts: PropTypes.func,
  chart_point: PropTypes.object
};

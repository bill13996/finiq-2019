import React, { Component } from "react";
import { Card } from "reactstrap";
import Button from "components/CustomButton/CustomButton";
import IMPORT from "assets/img/import-art.svg";
import PropTypes from "prop-types";
import uploadApi from "../../utils/uploadApi";
class CardImport extends Component {
  state = {};

  handleFileInput(e) {
    this.refs["uploadFile"].click(e);
  }

  async addFile(e) {
    let files = e.target.files;
    let fileArr = [];
    for (let i = 0; i < files.length; i++) {
      let file = e.target.files[i];
      const form_data = new FormData();
      form_data.append("file", file);
      form_data.append("accountID", "76774949");
      uploadApi("import-excel-data", form_data);
      localStorage.setItem("isImported", "true");
    }
    console.log(fileArr);
    this.handleLoader();
    setTimeout(() => {
      this.props.onImport();
    }, 1000);
  }

  handleLoader = () => {
    this.setState({
      isLoading: true
    });
    setTimeout(() => {
      this.setState({
        isLoading: false
      });
    }, 1000);
  };

  render() {
    const props = this.props;
    return (
      <>
        <Card className="card-import pr-0 d-flex justify-content-center">
          <div className="text-center">
            <img src={IMPORT} alt="" /> <br />
            <input
              type="file"
              name="fileUpload"
              className="inputFileHidden"
              ref="uploadFile"
              accept={".xlsx, xls"}
              style={{ zIndex: -1, display: "none" }}
              onChange={e => this.addFile(e)}
              multiple
            />
            <Button
              isLoading={this.state.isLoading}
              style={{
                backgroundColor: "#009fe3",
                color: "white",
                marginTop: "20px"
              }}
              onClick={e => this.handleFileInput(e)}
            >
              Tải file sao kê
            </Button>
          </div>
        </Card>
      </>
    );
  }
}
CardImport.propTypes = {
  content: PropTypes.string,
  onImport: PropTypes.func
};
export default CardImport;

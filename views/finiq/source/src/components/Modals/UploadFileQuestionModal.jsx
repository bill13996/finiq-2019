import React, { Component } from "react";
import {
  Modal,
  ModalHeader,
  ModalFooter,
  ModalBody,
  //   Input,
  ListGroupItem,
  ListGroup,
  Col,
  FormGroup,
  Row,
  Label
} from "reactstrap";
import Button from "components/CustomButton/CustomButton";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { actImportQuestionRequest } from "../../actions/managementgame.action";
import getUrlParam from "../../utils/getUrlParam";
import Dropzone from "dropzone";
Dropzone.autoDiscover = false;
class UploadFileQuestionModal extends Component {
  state = {
    moment: null,
    file: null
  };

  componentDidMount() {
    this.dropzoneInitial();
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    // nextProps.isUpdateTopic &&
    //   this.setState({ topic_info: nextProps.topic_info });
  }

  dropzoneInitial = () => {
    let currentMultipleFile = undefined;
    const seft = this;
    // multiple dropzone file - accepts any type of file
    this.dropzone = new Dropzone(document.getElementById("dropzone-multiple"), {
      url: "https://",
      thumbnailWidth: null,
      thumbnailHeight: null,
      previewsContainer: document.getElementsByClassName(
        "dz-preview-multiple"
      )[0],
      previewTemplate: document.getElementsByClassName("dz-preview-multiple")[0]
        .innerHTML,
      maxFiles: 1,
      acceptedFiles: null,
      init: function() {
        this.on("addedfile", function(file) {
          if (currentMultipleFile) {
            this.removeFile(currentMultipleFile);
          }
          currentMultipleFile = file;
          seft.setState({
            file: file
          });
        });
      }
    });
    document.getElementsByClassName("dz-preview-multiple")[0].innerHTML = "";
  };

  _clearData = () => {
    this.dropzone.removeAllFiles(true);
    this.setState({
      moment: null,
      file: null
    });
  };

  toggle = () => {
    this.props.toggle();
  };

  _hideAlert = () => {
    this.setState({
      alert: ""
    });
  };

  _handleOnUpload = () => {
    var form_data = new FormData();
    const file = this.state.file;
    console.log(file);
    form_data.append("file", file, file.name);
    this.props.ImportQuestion(form_data);
    this.props.toggle();
    this._setLoader();
    this._clearData();
  };

  _setLoader = () => {
    this.setState({ isLoading: true });
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 1000);
  };
  render() {
    return (
      <>
        {this.state.alert}
        <Modal
          isOpen={this.props.modal}
          toggle={this.toggle}
          className="modal-dialog-centered"
        >
          <ModalHeader toggle={this.props.toggle}>
            Upload file câu hỏi
          </ModalHeader>
          <ModalBody className="p-4">
            <FormGroup>
              <Label htmlFor="">Tải file</Label>
              <div
                className="dropzone dropzone-multiple"
                id="dropzone-multiple"
                style={{ zIndex: 0 }}
              >
                <div className="fallback">
                  <div className="custom-file">
                    <input
                      className="custom-file-input"
                      id="customFileUploadMultiple"
                      multiple="multiple"
                      type="file"
                    />
                    <label
                      className="custom-file-label"
                      htmlFor="customFileUploadMultiple"
                    >
                      Choose file
                    </label>
                  </div>
                </div>
                <ListGroup
                  className=" dz-preview dz-preview-multiple list-group-lg"
                  flush
                >
                  <ListGroupItem className=" px-0">
                    <Row className=" align-items-center">
                      <Col className=" col-auto">
                        <div className=" avatar">
                          <img
                            alt="..."
                            className=" avatar-img rounded"
                            data-dz-thumbnail
                            src="..."
                          />
                        </div>
                      </Col>
                      <div className=" col ml--3">
                        <h4 className=" mb-1" data-dz-name>
                          ...
                        </h4>
                        <p className=" small text-muted mb-0" data-dz-size>
                          ...
                        </p>
                      </div>
                      <Col className=" col-auto">
                        <Button size="sm" color="danger" data-dz-remove>
                          <i className="fas fa-trash" />
                        </Button>
                      </Col>
                    </Row>
                  </ListGroupItem>
                </ListGroup>
              </div>
            </FormGroup>
          </ModalBody>
          <ModalFooter className="p-4">
            <Button
              className="float-right"
              onClick={this._handleOnUpload}
              color="info"
            >
              Lưu
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    ImportQuestion: data => {
      dispatch(actImportQuestionRequest(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadFileQuestionModal);

UploadFileQuestionModal.propTypes = {
  //   topic_info: PropTypes.object,
  ImportQuestion: PropTypes.func
};

UploadFileQuestionModal.defaultProps = {};

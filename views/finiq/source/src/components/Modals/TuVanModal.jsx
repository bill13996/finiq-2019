import React, { Component } from "react";
import {
  FormGroup,
  Row,
  Col,
  Modal,
  ModalBody,
  Label,
  Input,
  Button
} from "reactstrap";
import TUVAN from "assets/img/tu-van.png";
import PropTypes from "prop-types";
import CLOSE from "assets/img/btn-close.svg";
import { formatMoney } from "utils/formatMoney";
import shuffleArray from "utils/shuffleArray";
import { products_down, products_tk_up } from "../../variables/data";
import { actAddDreamRequest } from "../../actions/dream.action";
import { connect } from "react-redux";
import { actGetMedianNumber } from "../../actions/account.action";

class TuVanModal extends Component {
  state = {
    isImported: true,
    monthly_money: 0,
    monthly_time: 0,
    median: 2000
  };

  componentDidMount() {
    this.props.getMedian();
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    console.log(nextProps.monthly_time, this.props.monthly_time);
    if (
      nextProps.monthly_money !== this.props.monthly_money ||
      nextProps.monthly_time !== this.props.monthly_time
    ) {
      const monthly_money = nextProps.monthly_money;
      const monthly_time = nextProps.monthly_time;
      this.setState({
        monthly_money,
        monthly_time
      });
    }
  }

  handleOnWantToLoan = () => {
    const goal_money = this.props.goal_money;
    const current_balance = this.props.current_balance;
    const median = this.props.median;
    this.setState({
      monthly_money: median,
      monthly_time:
        (goal_money - current_balance) /
        (median - (goal_money - current_balance) / 100),
      message: "Lãi suất 1%/tháng trên dư nợ ban đầu"
    });
  };

  handleOnBack = () => {
    const goal_money = this.props.goal_money;
    const current_balance = this.props.current_balance;
    const monthly_time = this.props.monthly_time;
    this.setState({
      monthly_money: (goal_money - current_balance) / monthly_time,
      monthly_time: monthly_time,
      message: ""
    });
  };

  renderTKProduct = () => {
    return (
      <div className="d-flex flex-wrap">
        {shuffleArray(products_tk_up).map((product, key) => {
          if (key < 4)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
      </div>
    );
  };

  renderVayProduct = () => {
    return (
      <div className="d-flex flex-wrap">
        {shuffleArray(products_down).map((product, key) => {
          if (key < 4)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
      </div>
    );
  };

  handleOnProductClick = url => {
    window.open(url, "_blank");
  };

  handleOnAddDream = () => {
    const data = {
      goalID: this.props.muctieuSelect.value,
      description: this.props.goal_desc,
      money: parseInt(this.state.monthly_money),
      month: parseInt(this.state.monthly_time)
    };
    this.props.addDream(data);
  };

  render() {
    return (
      <>
        <Modal
          isOpen={this.props.isOpen}
          toggle={this.props.toggle}
          className={"modal-tuvan " + this.props.className}
        >
          <img
            onClick={this.props.toggle}
            className="btn-close"
            src={CLOSE}
            alt=""
          />
          <ModalBody>
            <Row>
              <Col md="6">
                <img src={TUVAN} alt="" />
              </Col>
              <Col md="6">
                {this.state.message && (
                  <div
                    onClick={this.handleOnBack}
                    style={{ color: "blue", cursor: "pointer" }}
                  >
                    <span className="fas fa-arrow-left"></span>
                    {" Trở lại"}
                  </div>
                )}
                <FormGroup>
                  <Label>Số tiền cần mỗi tháng:</Label>
                  <Input
                    type="text"
                    name="monthly_money"
                    value={formatMoney(this.state.monthly_money) + " VNĐ"}
                    readOnly
                  ></Input>
                </FormGroup>
                <FormGroup>
                  <Label>Thời gian đạt được mục tiêu:</Label>
                  <Input
                    name="monthly_amount"
                    type="text"
                    value={formatMoney(this.state.monthly_time)}
                    readOnly
                  ></Input>
                </FormGroup>
                <FormGroup>
                  <Label>Tư vấn của FINIQ</Label>
                  {!this.state.message &&
                    (this.props.median > this.state.monthly_money ? (
                      <div className="tuvan-success">
                        Bạn có thể thực hiện mục tiêu này, cố lên nhé!
                      </div>
                    ) : (
                      <div className="tuvan-danger">
                        Bạn nên bổ sung thêm nguồn tài chính khác để thực hiện
                        mục tiêu!
                      </div>
                    ))}
                  <div>{this.state.message}</div>
                </FormGroup>
                <FormGroup>
                  <Button
                    style={{
                      color: "#001589",
                      backgroundColor: "#d0db00"
                    }}
                    onClick={this.handleOnAddDream}
                  >
                    Lưu mục tiêu
                  </Button>
                  <Button
                    onClick={this.handleOnWantToLoan}
                    style={{
                      color: "white",
                      backgroundColor: "#009fe3"
                    }}
                    disabled={this.state.message}
                  >
                    Tôi muốn vay
                  </Button>
                </FormGroup>
              </Col>
            </Row>
            <div
              style={{ borderBottom: "1px solid #ccc", margin: "30px 0" }}
            ></div>
            <Row>
              <div className="san-pham-goi-y pl-3">
                <div className="label">Có thể bạn quan tâm</div>
                {this.props.median > this.props.monthly_money
                  ? this.renderTKProduct()
                  : this.renderVayProduct()}
              </div>
            </Row>
          </ModalBody>
        </Modal>
      </>
    );
  }
}
TuVanModal.propTypes = {
  isOpen: PropTypes.bool,
  toggle: PropTypes.func,
  className: PropTypes.string
};
const mapStateToProps = state => {
  return {
    median: state.accountReducer.median
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addDream: data => {
      dispatch(actAddDreamRequest(data));
    },
    getMedian: () => {
      dispatch(actGetMedianNumber());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TuVanModal);

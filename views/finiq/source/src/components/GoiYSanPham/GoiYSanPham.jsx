import React, { Component } from "react";
import { Card, CardBody } from "reactstrap";
import PropTypes from "prop-types";
import shuffleArray from "utils/shuffleArray";
import {
  products_tk_up,
  products_car_up,
  products_home_up,
  products_down
} from "../../variables/data";

class GoiYSanPham extends Component {
  state = {
    isRadienUp: true
  };

  renderProductsUp = () => {
    return (
      <div className="d-flex flex-wrap">
        {shuffleArray(products_tk_up).map((product, key) => {
          if (key < 2)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
        {shuffleArray(products_car_up).map((product, key) => {
          return (
            <div
              className="sanpham cursor-pointer"
              onClick={() => this.handleOnProductClick(product.url)}
              key={key}
            >
              <div
                className="sanpham-image"
                style={{ backgroundImage: `url(${product.image})` }}
              ></div>
              <div className="sanpham-detail">
                <div className="heading">{product.title}</div>
                <div className="description">{product.description}</div>
              </div>
            </div>
          );
        })}
        {shuffleArray(products_home_up).map((product, key) => {
          if (key < 1)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
      </div>
    );
  };

  renderProductsDown = () => {
    return (
      <div className="d-flex flex-wrap">
        {shuffleArray(products_down).map((product, key) => {
          if (key < 2)
            return (
              <div
                className="sanpham cursor-pointer"
                onClick={() => this.handleOnProductClick(product.url)}
                key={key}
              >
                <div
                  className="sanpham-image"
                  style={{ backgroundImage: `url(${product.image})` }}
                ></div>
                <div className="sanpham-detail">
                  <div className="heading">{product.title}</div>
                  <div className="description">{product.description}</div>
                </div>
              </div>
            );
        })}
      </div>
    );
  };

  handleOnProductClick = url => {
    window.open(url, "_blank");
  };

  render() {
    const props = this.props;
    return (
      <>
        <Card class="card-goi-y-san-pham">
          <CardBody>
            <div className="san-pham-goi-y">
              <div className="label">Có thể bạn quan tâm</div>
              {this.state.isRadienUp
                ? this.renderProductsUp()
                : this.renderProductsDown()}
            </div>
          </CardBody>
        </Card>
      </>
    );
  }
}
GoiYSanPham.propTypes = {
  content: PropTypes.string
};
export default GoiYSanPham;

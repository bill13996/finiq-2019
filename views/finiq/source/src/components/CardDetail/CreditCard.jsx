import React, { Component } from "react";
import { formatMoney } from "utils/formatMoney";
import { Button } from "reactstrap";
import LOGO from "assets/img/LOGO_ACB.svg";
import LOGO_VISA from "assets/img/logo-visa.svg";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  actGetAccountRequest,
  actGetCardsRequest
} from "../../actions/account.action";
class CreditCard extends Component {
  state = {};

  componentDidMount() {
    this.props.getAccount();
    this.props.getCards();
  }

  handleLogOut = () => {
    localStorage.clear();
    this.props.history.push("/login");
  };

  render() {
    const props = this.props;
    const info = this.props.card.info;
    const iss = this.props.card.iss;
    const account = this.props.account;
    return (
      <>
        <div className="credit-card">
          <div className="credit-card__top">
            <div className="debit">Credit</div>
            <div className="logo">
              <img src={LOGO} alt="logo" />
            </div>
          </div>
          <div className="credit-card__center">
            <div className="number">
              <span>{info.display_card_number.substr(0, 4)}</span>
              <span>-</span>
              <span>{info.display_card_number.substr(4, 4)}</span>
              <span>-</span>
              <span>{info.display_card_number.substr(8, 4)}</span>
              <span>-</span>
              <span>{info.display_card_number.substr(12, 4)}</span>
            </div>
          </div>
          <div className="credit-card__bottom mt-3">
            <div className="name">{info.first_name + " " + info.last_name}</div>
            <div className="logo">
              <img src={LOGO_VISA} alt="" />
            </div>
          </div>
        </div>
        <div className="credit-card-detail">
          <table>
            <tr>
              <td>Dư nợ</td>
              <td className="text-right">{formatMoney(0)}</td>
            </tr>
            <tr>
              <td>Số dư khả dụng</td>
              <td className="text-right">{formatMoney(0)}</td>
            </tr>
            <tr>
              <td>Giới hạn tín dụng</td>
              <td className="text-right">
                {formatMoney(account.available_credit_limit)}
              </td>
            </tr>
            <tr>
              <td>Số tiền cần phải thanh toán</td>
              <td className="text-right">
                {account.amount_due && formatMoney(account.amount_due.num)}
              </td>
            </tr>
          </table>
        </div>
        <div className="credit-card-status">
          <table>
            <tr>
              <td className="td-label" colSpan="2">
                <div className="label">Thông tin thẻ</div>
              </td>
            </tr>
            <tr>
              <td>Tình trạng</td>
              <td className="text-right">
                {info.status_detail === "Card OK"
                  ? "Hoạt động"
                  : "Ngừng hoạt động"}
              </td>
            </tr>
            <tr>
              <td>Loại thẻ</td>
              <td className="text-right">VISA</td>
            </tr>
            <tr>
              <td>Loại tiền</td>
              <td className="text-right">
                Vietnam Đồng ( <span>đ</span> )
              </td>
            </tr>
          </table>
        </div>
        <div className="button-container text-right">
          <Button
            onClick={this.handleOnLogout}
            style={{ backgroundColor: "#cfdb00", color: "#001589" }}
          >
            Đăng xuất
          </Button>
        </div>
      </>
    );
  }
}
CreditCard.propTypes = {
  content: PropTypes.string
};

const mapStateToProps = state => {
  return {
    card: state.accountReducer.card,
    account: state.accountReducer.account
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCards: () => {
      dispatch(actGetCardsRequest());
    },
    getAccount: () => {
      dispatch(actGetAccountRequest());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreditCard);

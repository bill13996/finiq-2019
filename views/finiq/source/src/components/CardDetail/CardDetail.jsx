import React, { Component } from "react";
import { Container } from "reactstrap";
import PropTypes from "prop-types";
import CreditCard from "./CreditCard";
class CardDetail extends Component {
  state = {};
  render() {
    const props = this.props;
    return (
      <>
        <div className="card-detail">
          <CreditCard />
        </div>
      </>
    );
  }
}
CardDetail.propTypes = {
  content: PropTypes.string
};
export default CardDetail;

import React, { Component } from "react";
import { Container, Col, Button } from "reactstrap";
import PropTypes from "prop-types";
import GoiYSanPham from "../../components/GoiYSanPham/GoiYSanPham";
import FormMucTieu from "../../components/Forms/FormMucTieu";
class ThietLapMucTieu extends Component {
  state = {
    isImported: localStorage.getItem("isImported") === "true"
  };

  componentDidMount() {
    this.checkImported();
  }

  checkImported = () => {
    if (!this.state.isImported) {
      this.props.history.push("/thoi-quen-chi-tieu");
    }
  };

  render() {
    const props = this.props;
    return (
      <>
        <Container fluid className="animated fadeIn">
          <FormMucTieu />
          <GoiYSanPham />
        </Container>
      </>
    );
  }
}
ThietLapMucTieu.propTypes = {
  content: PropTypes.string
};
export default ThietLapMucTieu;

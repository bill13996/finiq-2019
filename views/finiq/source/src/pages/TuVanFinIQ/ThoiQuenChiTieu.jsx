import React, { Component } from "react";
import { Container } from "reactstrap";
import PropTypes from "prop-types";
import CardImport from "../../components/CardImport/CardImport";
import HoatDongChiTieu from "../../components/HoatDongChiTieu/HoatDongChiTieu";
class ThoiQuenChiTieu extends Component {
  state = {
    isImported: localStorage.getItem("isImported") === "true"
  };

  handleOnImport = () => {
    this.setState({
      isImported: true
    });
  };

  render() {
    const props = this.props;
    return (
      <>
        <Container fluid className="pr-0">
          {!this.state.isImported && (
            <CardImport onImport={this.handleOnImport} />
          )}
          {this.state.isImported && <HoatDongChiTieu />}
        </Container>
      </>
    );
  }
}
ThoiQuenChiTieu.propTypes = {
  content: PropTypes.string
};
export default ThoiQuenChiTieu;

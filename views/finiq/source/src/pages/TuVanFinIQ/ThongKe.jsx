import React, { Component } from "react";
import { Container, Col, Button } from "reactstrap";
import CardImport from "../../components/CardImport/CardImport";
import PropTypes from "prop-types";
import ThongKeChiTiet from "../../components/ThongKeChiTiet/ThongKeChiTiet";
class ThongKe extends Component {
  state = {
    isImported: localStorage.getItem("isImported") === "true"
  };

  componentDidMount() {
    this.checkImported();
  }

  checkImported = () => {
    if (!this.state.isImported) {
      this.props.history.push("/thoi-quen-chi-tieu");
    }
  };

  render() {
    const props = this.props;
    return (
      <>
        <Container fluid>
          {!this.state.isImported && <CardImport />}
          {this.state.isImported && <ThongKeChiTiet chart-type="chi-tiet" />}
        </Container>
      </>
    );
  }
}
ThongKe.propTypes = {
  content: PropTypes.string
};
export default ThongKe;

import axios from "axios";
import HOST_ACB from "variables/hostACB";
import accessToken from "../variables/accessToken";

export default function callACBApi(
  endpoint,
  method = "GET",
  data = null,
  form_data = false
) {
  return axios({
    method: method,
    url: `${HOST_ACB}${endpoint}`,
    data: data,
    headers: {
      Authorization: "Bearer " + accessToken,
      "content-type": !form_data
        ? "application/json;charset=UTF-8"
        : "multipart/form-data"
    }
  })
    .then(res => {
      return res.data;
    })
    .catch(err => {
      // eslint-disable-next-line no-console
      console.log(err);
      throw err;
    });
}

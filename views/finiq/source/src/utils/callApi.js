import axios from "axios";
import HOST from "variables/host";

export default function callApi(
  endpoint,
  method = "GET",
  data = null,
  form_data = false
) {
  return axios({
    method: method,
    url: `${HOST}${endpoint}`,
    data: data,
    headers: {
      accesstoken: localStorage.getItem("accesstoken"),
      "content-type": !form_data
        ? "application/json;charset=UTF-8"
        : "multipart/form-data"
    }
  })
    .then(res => {
      return res.data;
    })
    .catch(err => {
      // eslint-disable-next-line no-console
      console.log(err);
      const status = err.response && err.response.data.status;
      if (status === 403 && window.location.href !== "/admin/login")
        window.location.href = "/admin/login";
      throw err;
    });
}

import axios from "axios";
import HOST from "../variables/host";
export default function uploadApi(endpoint, formData) {
  return axios({
    method: "POST",
    url: `${HOST}${endpoint}`,
    data: formData,
    headers: {
      "content-type": "multipart/form-data",
      accesstoken: localStorage.getItem("accesstoken")
    }
  }).then(res => res.data);
}

import React from "react";
import { Admin } from "../Admin";
import { shallow } from "enzyme";
const location = { pathname: "/" };
const history = [];

describe("First React component test with Enzyme", () => {
  it("renders without crashing", () => {
    shallow(<Admin location={location} history={history} />);
  });
});

import React from "react";
// react library for routing
import { Route, Switch } from "react-router-dom";

// core components
import AuthFooter from "components/Footers/AuthFooter.jsx";
import { connect } from "react-redux";
import routes from "routes.js";
import { actResetFetchResource } from "../actions/fetch.action";
import AlertSuccess from "components/SweetAlert/AlertSuccess";
import AlertError from "components/SweetAlert/AlertError";
import BG from "assets/img/login-background.svg";

class Auth extends React.Component {
  componentDidMount() {
    document.body.classList.add("bg-default");
  }
  componentWillUnmount() {
    document.body.classList.remove("bg-default");
  }
  componentDidUpdate(e) {
    if (e.history.pathname !== e.location.pathname) {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainContent.scrollTop = 0;
    }
  }
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    if (nextProps.fetchSuccess) {
      this.setState({ fetchSuccess: true });
    } else if (nextProps.fetchSuccess === null) {
      this.setState({ fetchFail: false });
    } else if (nextProps.fetchFail) {
      this.setState({ fetchFail: true });
    } else if (nextProps.fetchFail) {
      this.setState({ fetchFail: false });
    }
    if (nextProps.notify !== null && nextProps.notify !== this.props.notify) {
      this.notify(nextProps.notify.message, nextProps.notify.color);
    }
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.collapse) {
        return this.getRoutes(prop.views);
      }
      if (prop.layout === "/login") {
        return <Route path={prop.path} component={prop.component} key={key} />;
      } else {
        return null;
      }
    });
  };
  render() {
    const props = this.props;
    return (
      <>
        {props.fetchSuccess && (
          <AlertSuccess
            {...props}
            message={props.fetchSuccess.message}
            confirmTo={props.fetchSuccess.confirmTo}
            resetFetchResource={props.resetFetchResource}
          />
        )}
        {props.fetchFail && (
          <AlertError
            {...props}
            message={props.fetchFail.message}
            confirmTo={props.fetchFail.confirmTo}
            resetFetchResource={props.resetFetchResource}
          />
        )}
        <div
          className="main-content"
          style={{
            display: "flex",
            justifyContent: "center",
            minHeight: "100vh",
            backgroundColor: "white",
            backgroundImage: `url(${BG})`,
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
          ref="mainContent"
        >
          <Switch>{this.getRoutes(routes)}</Switch>
        </div>
        <AuthFooter />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    fetchSuccess: state.fetchReducer.fetchSuccess,
    fetchFail: state.fetchReducer.fetchFail,
    notify: state.fetchReducer.notify
  };
};

const mapDispatchToProps = dispatch => {
  return {
    resetFetchResource: () => {
      dispatch(actResetFetchResource());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth);

import React from "react";
// react library for routing
import { Route, Switch, Redirect } from "react-router-dom";
import routes from "routes.js";
// core components
import { Row, Col } from "reactstrap";
import NotificationAlert from "react-notification-alert";
import AdminFooter from "components/Footers/AdminFooter.jsx";
import Sidebar from "components/Sidebar/Sidebar.jsx";
import AdminNavbar from "../components/Navbars/AdminNavbar";
import AlertSuccess from "components/SweetAlert/AlertSuccess";
import AlertError from "components/SweetAlert/AlertError";
//actions
import { connect } from "react-redux";
import { actResetFetchResource } from "../actions/fetch.action";
import CardDetail from "../components/CardDetail/CardDetail";

export class Admin extends React.Component {
  state = {
    sidenavOpen: true
  };

  componentDidMount() {
    this.checkLogin();
  }

  componentDidUpdate(e) {
    if (e.history.pathname !== e.location.pathname) {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainContent.scrollTop = 0;
    }
  }
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    if (nextProps.fetchSuccess) {
      this.setState({ fetchSuccess: true });
    } else if (nextProps.fetchSuccess === null) {
      this.setState({ fetchFail: false });
    } else if (nextProps.fetchFail) {
      this.setState({ fetchFail: true });
    } else if (nextProps.fetchFail) {
      this.setState({ fetchFail: false });
    }
    if (nextProps.notify !== null && nextProps.notify !== this.props.notify) {
      this.notify(nextProps.notify.message, nextProps.notify.color);
    }
  }

  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.collapse) {
        return this.getRoutes(prop.views);
      }
      if (prop.redirect) {
        return <Redirect from={prop.path} to={prop.pathTo} key={key} />;
      }
      if (prop.layout === "/") {
        return (
          <Route
            path={prop.path}
            exact={prop.exact}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };

  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };

  toggleSidenav = e => {
    if (document.body.classList.contains("g-sidenav-pinned")) {
      document.body.classList.remove("g-sidenav-pinned");
      document.body.classList.add("g-sidenav-hidden");
    } else {
      document.body.classList.add("g-sidenav-pinned");
      document.body.classList.remove("g-sidenav-hidden");
    }
    this.setState({
      sidenavOpen: !this.state.sidenavOpen
    });
  };

  getNavbarTheme = () => {
    return this.props.location.pathname.indexOf(
      "admin/alternative-dashboard"
    ) === -1
      ? "dark"
      : "light";
  };

  checkLogin = () => {
    if (!localStorage.getItem("accesstoken")) {
      this.props.history.push("/login");
    }
  };

  notify(message, color) {
    let options = {
      place: "tr",
      message: (
        <div className="alert-text">
          <span data-notify="message">{message}</span>
        </div>
      ),
      type: color,
      icon: "ni ni-bell-55",
      autoDismiss: 7
    };
    this.refs.notificationAlert.notificationAlert(options);
  }

  render() {
    const props = this.props;
    return (
      <>
        <NotificationAlert ref="notificationAlert" />
        {props.fetchSuccess && (
          <AlertSuccess
            {...props}
            message={props.fetchSuccess.message}
            confirmTo={props.fetchSuccess.confirmTo}
            resetFetchResource={props.resetFetchResource}
          />
        )}
        {props.fetchFail && (
          <AlertError
            {...props}
            message={props.fetchFail.message}
            confirmTo={props.fetchFail.confirmTo}
            resetFetchResource={props.resetFetchResource}
          />
        )}
        <Sidebar
          {...this.props}
          routes={routes}
          toggleSidenav={this.toggleSidenav}
          sidenavOpen={this.state.sidenavOpen}
          logo={{
            innerLink: "/",
            imgSrc: require("../assets/img/brand/csr-logo.png"),
            imgAlt: "..."
          }}
        />

        <div
          className="main-content d-flex"
          // eslint-disable-next-line react/no-string-refs
          ref="mainContent"
          onClick={this.closeSidenav}
        >
          <div className={"main-content-container"}>
            <AdminNavbar
              {...this.props}
              theme={this.getNavbarTheme()}
              toggleSidenav={this.toggleSidenav}
              sidenavOpen={this.state.sidenavOpen}
              brandText={this.getBrandText(this.props.location.pathname)}
            />
            <Row>
              <Col md="8" lg="9">
                <Switch>{this.getRoutes(routes)}</Switch>
              </Col>
              <Col md="4" lg="3">
                <CardDetail />
              </Col>
            </Row>
            <AdminFooter />
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    fetchSuccess: state.fetchReducer.fetchSuccess,
    fetchFail: state.fetchReducer.fetchFail,
    notify: state.fetchReducer.notify
  };
};

const mapDispatchToProps = dispatch => {
  return {
    resetFetchResource: () => {
      dispatch(actResetFetchResource());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Admin);

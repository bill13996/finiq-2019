import React from "react";
import { mount } from "enzyme";
import Login from "../admin/login";
import { Provider } from "react-redux";
import appReducers from "reducers/index.reducer";
import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose } from "redux";
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(appReducers, composeEnhancer(applyMiddleware(thunk)));

it("Render without crashing", () => {
  mount(
    <Provider store={store}>
      <Login />
    </Provider>
  );
});

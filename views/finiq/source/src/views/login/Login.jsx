import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col
} from "reactstrap";

import Button from "components/CustomButton/CustomButton";
// core components
import { actLoginRequest } from "../../actions/authentication.action";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import LOGO from "../../assets/img/logo-finiq.svg";

class Login extends React.Component {
  state = {
    isLoading: false
  };

  componentDidMount() {
    this.checkRememberMe();
  }

  handleOnChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleOnChangeRemember = () => {
    this.setState(prevState => ({
      isRememberMe: !prevState.isRememberMe
    }));
  };

  handleOnSubmit = () => {
    const adminName = this.state.adminName;
    const password = this.state.password;
    const isRememberMe = this.state.isRememberMe;
    this.toogleLoading();
    if (!adminName || !password) {
      this.setState({
        message: "* Email or password cannot be blank!"
      });
      this.toogleLoading();
    } else {
      const data = {
        adminName,
        password,
        isRememberMe
      };
      setTimeout(() => {
        this.props.login(data);
      }, 500);
    }
  };

  checkRememberMe = () => {
    if (
      localStorage.getItem("accesstoken") &&
      localStorage.getItem("is_remember") === "1"
    ) {
      this.props.history.push("/banner");
    }
  };

  onKeyDown = (event: React.KeyboardEvent<HTMLDivElement>): void => {
    if (event.key === "Enter") {
      event.preventDefault();
      event.stopPropagation();
      this.handleOnSubmit();
    }
  };

  toogleLoading = () => {
    this.setState({
      isLoading: true
    });
    setTimeout(() => {
      this.setState({
        isLoading: false
      });
    }, 1000);
  };

  render() {
    return (
      <>
        <Container
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Row style={{ width: "100%", justifyContent: "center" }}>
            <Col lg="12" md="12">
              <div className="text-center" style={{ width: "100%" }}>
                <img
                  src={LOGO}
                  alt="csr-logo"
                  style={{ marginLeft: "-160px" }}
                />
              </div>
            </Col>
            <Col lg="5" md="7">
              <Card className="bg-secondary border-0 mb-0">
                <CardHeader className="bg-transparent"></CardHeader>
                <CardBody>
                  <Form role="form">
                    <FormGroup
                      className={classnames("mb-3", {
                        focused: this.state.focusedEmail
                      })}
                    >
                      <InputGroup className="input-group-merge input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-circle-08" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Tên truy cập"
                          type="text"
                          name="adminName"
                          onFocus={() => this.setState({ focusedEmail: true })}
                          onBlur={() => this.setState({ focusedEmail: false })}
                          onChange={this.handleOnChange}
                          value={this.state.userName}
                          onKeyDown={this.onKeyDown}
                          autoComplete="new-password"
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup
                      className={classnames({
                        focused: this.state.focusedPassword
                      })}
                    >
                      <InputGroup className="input-group-merge input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-lock-circle-open" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="password"
                          placeholder="Password"
                          type="password"
                          onFocus={() =>
                            this.setState({ focusedPassword: true })
                          }
                          onBlur={() =>
                            this.setState({ focusedPassword: false })
                          }
                          autoComplete="new-password"
                          onChange={this.handleOnChange}
                          value={this.state.password}
                          onKeyDown={this.onKeyDown}
                        />
                      </InputGroup>
                    </FormGroup>
                    <div className="text-danger mt-3 mb-3">
                      {this.state.message}
                    </div>
                    <div className="custom-control custom-control-alternative custom-checkbox">
                      <input
                        className="custom-control-input"
                        id=" customCheckLogin"
                        type="checkbox"
                        onClick={this.handleOnChangeRemember}
                        checked={this.state.isRememberMe}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor=" customCheckLogin"
                      >
                        <span className="text-muted">Remember me</span>
                      </label>
                    </div>
                    <div className="text-center">
                      <Button
                        className="my-4"
                        onClick={this.handleOnSubmit}
                        type="button"
                        isLoading={this.state.isLoading}
                        style={{
                          backgroundColor: "#009fe3",
                          borderColor: "#e3e966",
                          color: "white"
                        }}
                      >
                        Đăng nhập
                      </Button>
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    login: data => {
      dispatch(actLoginRequest(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

Login.propTypes = {
  history: PropTypes.object,
  login: PropTypes.func
};

import Login from "./views/login/Login";
import TuVan from "./pages/TuVanFinIQ/ThoiQuenChiTieu";
import ThongKe from "./pages/TuVanFinIQ/ThongKe";
import ICON_TUVAN from "assets/img/icon-tuvan.svg";
import ICON_MUCTIEU from "assets/img/icon-muctieu.svg";
import ThietLapMucTieu from "./pages/MucTieu/ThietLapMucTieu";
import GoiYMucTieu from "./pages/MucTieu/QuanLyMucTieu";
import QuanLyMucTieu from "./pages/MucTieu/QuanLyMucTieu";
import ThoiQuenChiTieu from "./pages/TuVanFinIQ/ThoiQuenChiTieu";

const routes = [
  {
    collapse: true,
    name: "Tư vấn FINIQ",
    icon: ICON_TUVAN,
    state: "managementgameCollapse",
    views: [
      {
        path: "/thoi-quen-chi-tieu",
        name: "Thói quen chi tiêu",
        layout: "/",
        exact: true,
        component: ThoiQuenChiTieu
      },
      {
        path: "/thong-ke-chi-tiet",
        name: "Thống kê chi tiết",
        layout: "/",
        exact: true,
        component: ThongKe
      }
    ]
  },
  {
    collapse: true,
    name: "Mục tiêu của tôi",
    icon: ICON_MUCTIEU,
    state: "managemengameCollapse",
    views: [
      {
        path: "/thiet-lap-muc-tieu",
        name: "Thiết lập mục tiêu",
        layout: "/",
        exact: true,
        component: ThietLapMucTieu
      },
      {
        path: "/quan-ly-muc-tieu",
        name: "Quản lý mục tiêu",
        layout: "/",
        exact: true,
        component: QuanLyMucTieu
      }
    ]
  },
  {
    name: "Login",
    path: "/login",
    layout: "/login",
    invisible: true,
    component: Login
  },
  {
    redirect: true,
    path: "/",
    pathTo: "/thoi-quen-chi-tieu",
    invisible: true
  }
];

export default routes;

import G101 from "assets/img/TREN_TRUNVI/TIETKIEM/G101.png";
import GD05 from "assets/img/TREN_TRUNVI/TIETKIEM/GD05.png";
import GD08 from "assets/img/TREN_TRUNVI/TIETKIEM/GD08.png";
import GD47 from "assets/img/TREN_TRUNVI/TIETKIEM/GD47.png";
import KE20 from "assets/img/TREN_TRUNVI/TIETKIEM/KE20.png";
import KE33 from "assets/img/TREN_TRUNVI/TIETKIEM/KE33.png";
import KF01 from "assets/img/TREN_TRUNVI/TIETKIEM/KF01.png";

import DN02 from "assets/img/TREN_TRUNVI/VAYMUAOTO/DN02.png";

import NN06 from "assets/img/TREN_TRUNVI/VAYMUANHA/NN06.png";
import NN03 from "assets/img/TREN_TRUNVI/VAYMUANHA/NN03.png";
import NN05 from "assets/img/TREN_TRUNVI/VAYMUANHA/NN05.png";

import CT01 from "assets/img/DUOI_TRUNGVI/CT01.png";
import CT02 from "assets/img/DUOI_TRUNGVI/CT02.png";

const products_tk_up = [
  {
    product: "G101",
    title: "Tiết kiệm có kỳ hạn",
    image: G101,
    description: "Thỏa mãn nhu cầu chi tiêu cá nhân dễ dàng và nhanh chóng",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/tiet-kiem-co-ky-han"
  },
  {
    product: "GD05",
    title: "Tích lũy thiên thần nhỏ",
    image: GD05,
    description: "Thỏa mãn nhu cầu chi tiêu cá nhân dễ dàng và nhanh chóng",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/tich-luy-thien-than-nho"
  },
  {
    product: "GD08",
    title: "Tích lũy an cư lập nghiệp",
    image: GD08,
    description:
      "Sẵn sàng cho dự định của gia đình bằng nguồn tài chính được vun đắp ở ACB",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/tich-luy-an-cu-lap-nghiep"
  },
  {
    product: "GD47",
    title: "Tích lũy thành tài vững bước tương lai",
    image: GD47,
    description: "Một sản phẩm của dịch vụ tài chính cho gia đình Việt",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/tich-luy-thanh-tai-vung-buoc-tuong-lai"
  },
  {
    product: "KE20",
    image: KE20,
    title: "Lộc bảo toàn",
    description:
      "Lộc Bảo Toàn là sản phẩm tiết kiệm có kì hạn liên kết với công ty bảo hiểm Prévior Vietnam",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/loc-bao-toan"
  },
  {
    product: "KE33",
    image: KE33,
    title: "Đại lộc",
    description: "Là dòng sản phẩm tiết kiệm chuyên biệt với ưu đãi vượt trội",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/dai-loc"
  },
  {
    product: "KF01",
    image: KF01,
    title: "Phúc An Lộc",
    description:
      "Dành riêng cho khách hàng tuổi từ 50+, chăm sóc sức khỏe, tận hưởng cuộc sống",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/tiet-kiem-phuc-an-loc-2-1-2018"
  }
];

const products_car_up = [
  {
    product: "DN02",
    image: DN02,
    title: "Vay mua xe ô tô",
    url: "https://www.acb.com.vn/vn/personal/cho-vay/vay-tieu-dung/mua-xe-oto",
    description: "Đã ước mơ sao phải đợi"
  }
];

const products_home_up = [
  {
    product: "NN06",
    image: NN06,
    title: "Vay mua nhà đất",
    url:
      "https://www.acb.com.vn/vn/personal/cho-vay/vay-mua-nha/vay-mua-nha-dat",
    description: "Bước gần hơn đến ngôi nhà trong mơ và gia đình"
  },
  {
    product: "NN03",
    image: NN03,
    title: "Vay xâu sửa nhà",
    url:
      "https://www.acb.com.vn/vn/personal/cho-vay/vay-mua-nha/vay-xay-sua-nha",
    description:
      "Nhằm hỗ trợ nguồn vốn giúp khách hàng thanh toán các chi phí xây dựng sửa chữa"
  },
  {
    product: "NN05",
    image: NN05,
    title: "Vay mua căn hộ dự án",
    url:
      "https://www.acb.com.vn/vn/personal/cho-vay/vay-mua-nha/vay-can-ho-du-an",
    description: "Thỏa mãn ước mơ sở hữu căn hộ cao cấp và dự án bất động sản"
  }
];

const products_down = [
  {
    product: "CT01",
    image: CT01,
    title: "Vay tiêu dùng linh hoạt",
    url:
      "https://www.acb.com.vn/vn/personal/cho-vay/vay-tieu-dung/vay-tieu-dung-linh-hoat",
    description: "Làm chủ cuộc sống xứng tầm"
  },
  {
    product: "CT02",
    image: CT02,
    title: "Vay tín chấp",
    url:
      "https://www.acb.com.vn/vn/personal/cho-vay/vay-tieu-dung/vay-tin-chap",
    description: "48 giờ cho mọi dự định với lãi suất ưu đãi"
  },
  {
    product: "GD05",
    title: "Tích lũy thiên thần nhỏ",
    image: GD05,
    description: "Thỏa mãn nhu cầu chi tiêu cá nhân dễ dàng và nhanh chóng",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/tich-luy-thien-than-nho"
  },
  {
    product: "GD08",
    title: "Tích lũy an cư lập nghiệp",
    image: GD08,
    description:
      "Sẵn sàng cho dự định của gia đình bằng nguồn tài chính được vun đắp ở ACB",
    url:
      "https://www.acb.com.vn/vn/personal/tai-khoan-tien-gui/tien-gui-tiet-kiem/tich-luy-an-cu-lap-nghiep"
  }
];

export { products_tk_up, products_home_up, products_car_up, products_down };

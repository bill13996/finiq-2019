var HOST_URL = "";
if (
  window.location.hostname === "localhost" ||
  window.location.hostname === "[::1]" ||
  window.location.hostname.match(
    /^192(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
  )
) {
  // HOST = "/cms/api";
  HOST_URL = "https://staging.ganlaio.com/";
} else {
  HOST_URL = window.location.protocol + "//" + window.location.hostname + "/";
}

export default HOST_URL;

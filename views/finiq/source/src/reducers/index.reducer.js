import { combineReducers } from "redux";
import fetchReducer from "./fetch.reducer";
import productReducer from "./product.reducer";
import importReducer from "./import.reducer";
import accountReducer from "./account.reducer";

const appReducers = combineReducers({
  fetchReducer,
  importReducer,
  productReducer,
  accountReducer,
  accountReducer
});

export default appReducers;

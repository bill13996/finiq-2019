import * as Types from "../variables/actionTypes";

var initialState = {
  imported: []
};

const importReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.ADD_IMPORTED_MODULE:
      var imported = state.imported;
      imported.push("Employee");
      state = { ...state, imported };
      return { ...state };
    default:
      return { ...state };
  }
};

export default importReducer;

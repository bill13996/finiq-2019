import * as Types from "../variables/actionTypes";

var initialState = {
  admin: {
    docs: []
  },
  admin_detail: {},
  admin_info: {}
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_ADMIN:
      var admin = action.data;
      state = { ...state, admin };
      return { ...state };
    case Types.GET_ADMIN_DETAIL:
      var admin_detail = action.data;
      state = { ...state, admin_detail };
      return { ...state };
    case Types.GET_INFO_ADMIN:
      var admin_info = action.data;
      state = { ...state, admin_info };
      return { ...state };
    default:
      return { ...state };
  }
};

export default adminReducer;

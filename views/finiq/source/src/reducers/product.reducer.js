import * as Types from "../variables/actionTypes";

var initialState = {
  products: []
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_PRODUCTS:
      state = { ...state, products: action.data.products };
      return { ...state };
    default:
      return { ...state };
  }
};

export default productReducer;

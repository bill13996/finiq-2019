import * as Types from "../variables/actionTypes";
import { chartTinhDiem, chartChiTiet } from "variables/charts";

var initialState = {
  card: {
    info: {
      display_card_number: ""
    },
    iss: {}
  },
  account: {
    current_balance: {},
    available_balance: {}
  },
  pointData: {},
  chart_point: {},
  chart_money: {},
  top3BestMoneyMonth: [],
  top3WorstMoneyMonth: [],
  dreams: [],
  median: 0
};

const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_ACCOUNT:
      state = { ...state, account: action.data };
      return { ...state };
    case Types.GET_CARD:
      state = { ...state, card: action.data };
      return { ...state };
    case Types.GET_CHART_POINT:
      var pointRotation = [];
      var sum = action.data.data.reduce((a, b) => a + b);
      var radien = sum / 12;
      action.data.data.forEach(value => {
        if (value >= radien) {
          pointRotation.push(0);
        } else {
          pointRotation.push(180);
        }
      });
      var chart_point = {
        options: chartTinhDiem.options,
        data: {
          labels: chartTinhDiem.data.labels,
          datasets: [
            {
              ...chartTinhDiem.data.datasets[0],
              data: action.data.data,
              pointRotation
            },
            {
              ...chartTinhDiem.data.datasets[1],
              data: [
                radien,
                radien,
                radien,
                radien,
                radien,
                radien,
                radien,
                radien,
                radien,
                radien,
                radien,
                radien
              ]
            }
          ]
        }
      };
      state = { ...state, chart_point };
      return { ...state };
    case Types.GET_CHART_MONEY_BY_YEAR:
      var data = action.data.data.list_month;
      var chart_money = {
        options: chartChiTiet.options,
        data: {
          labels: chartChiTiet.data.labels,
          datasets: [
            {
              ...chartChiTiet.data.datasets[0],
              data
            },
            {
              ...chartChiTiet.data.datasets[1],
              data: [
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance,
                action.data.data.median_balance
              ]
            }
          ]
        }
      };
      state = {
        ...state,
        chart_money
      };
      return { ...state };
    case Types.GET_STATISTIC_MONTH:
      var list_month_1 = action.data.data.list_month;
      state = {
        ...state,
        top3BestMoneyMonth: list_month_1
          .sort((a, b) => b - a)
          .slice(0, 3)
          .map(prop => {
            return list_month_1.indexOf(prop);
          }),
        top3WorstMoneyMonth: list_month_1
          .sort((a, b) => a - b)
          .slice(0, 3)
          .map(prop => {
            return list_month_1.indexOf(prop);
          })
      };
      return state;
    case Types.GET_DREAMS:
      state = {
        ...state,
        dreams: action.data.data
      };
      return { ...state };
    case Types.GET_MEDIAN_NUMBER:
      state = {
        ...state,
        median: action.data.data
      };
      return { ...state };
    default:
      return { ...state };
  }
};

export default accountReducer;

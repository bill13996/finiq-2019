// import * as Types from "../variables/actionTypes";
import { actFetchResourceSuccess, actFetchResourceFail } from "./fetch.action";
import accessToken from "../variables/accessToken";

export const actLoginRequest = data => {
  return dispatch => {
    const adminName = data.adminName;
    const password = data.password;
    if (adminName === "TRONGDUCNGUYEN" && password === "acb@123") {
      localStorage.setItem("accesstoken", accessToken);
      localStorage.setItem("adminName", "TRONGDUCNGUYEN");
      dispatch(
        actFetchResourceSuccess({
          message: "Chúc mừng bạn đã đăng nhập thành công!",
          confirmTo: "/admin/"
        })
      );
    } else if (adminName !== "TRONGDUCNGUYEN" && password !== "acb@123") {
      actFetchResourceFail({
        message: "Username hoặc password không hợp lệ, vui lòng thử lại sau!",
        confirmTo: window.location.pathname
      });
    }
  };
};

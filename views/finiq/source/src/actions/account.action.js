// import * as Types from "../variables/actionTypes";
import callApi from "utils/callApi";
import callACBApi from "../utils/callACBApi";
import * as Types from "variables/actionTypes";

export const actGetCardsRequest = data => {
  return dispatch => {
    return callACBApi("v1/profile/card").then(res => {
      callACBApi(`v1/card/${res.cards[0].card_token}`).then(res1 => {
        dispatch({
          type: Types.GET_CARD,
          data: res1
        });
      });
    });
  };
};

export const actGetAccountRequest = data => {
  return dispatch => {
    return callACBApi("v1/account/76774949?display_balance=true").then(res => {
      dispatch({
        type: Types.GET_ACCOUNT,
        data: res
      });
    });
  };
};

export const actGetChartPointRequest = () => {
  return dispatch => {
    return callApi("get-data-median-for-chart?accountID=76774949").then(res => {
      dispatch({
        type: Types.GET_CHART_POINT,
        data: res
      });
    });
  };
};

export const actGetChartMoneyByYearRequest = year => {
  return dispatch => {
    return callApi(
      `get-medium-balance-every-month-by-year?year=${year}&accountID=76774949`
    ).then(res => {
      dispatch({
        type: Types.GET_CHART_MONEY_BY_YEAR,
        data: res
      });
    });
  };
};

export const actGetStatMonthRequest = year => {
  return dispatch => {
    return callApi(
      `get-medium-balance-every-month-by-year?year=${year}&accountID=76774949`
    ).then(res => {
      dispatch({
        type: Types.GET_STATISTIC_MONTH,
        data: res
      });
    });
  };
};

export const actGetMedianNumber = () => {
  return dispatch => {
    return callApi(`get-median-balance-number?accountID=76774949`).then(res => {
      dispatch({
        type: Types.GET_MEDIAN_NUMBER,
        data: res
      });
    });
  };
};

// import * as Types from "../variables/actionTypes";
import { actNotify } from "./fetch.action";
import callACBApi from "../utils/callACBApi";
import * as Types from "variables/actionTypes";

export const actGetProductRequest = data => {
  return dispatch => {
    return callACBApi(
      `v1/service/product?product_class=${data.product_class}&product_group=${data.product_group}`
    )
      .then(res => {
        dispatch({
          type: Types.GET_PRODUCTS,
          data: res
        });
      })
      .catch(err => {
        dispatch(
          actNotify({
            message: "Đã có lỗi xảy ra vui lòng thử lại sau!",
            color: "danger"
          })
        );
      });
  };
};

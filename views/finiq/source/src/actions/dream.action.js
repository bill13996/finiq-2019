// import * as Types from "../variables/actionTypes";
import { actNotify, actFetchResourceSuccess } from "./fetch.action";
import callApi from "../utils/callApi";
import * as Types from "variables/actionTypes";

export const actGetDreamRequest = () => {
  return dispatch => {
    return callApi(`get-list-goal`)
      .then(res => {
        dispatch({
          type: Types.GET_DREAMS,
          data: res
        });
      })
      .catch(err => {
        dispatch(
          actNotify({
            message: "Đã có lỗi xảy ra vui lòng thử lại sau!",
            color: "danger"
          })
        );
      });
  };
};

export const actAddDreamRequest = data => {
  return dispatch => {
    return callApi(`create-goal`, "POST", data)
      .then(res => {
        dispatch(
          actFetchResourceSuccess({
            message: "Bạn đã lưu thành công mục tiêu mới",
            confirmTo: "/quan-ly-muc-tieu"
          })
        );
      })
      .catch(err => {
        dispatch(
          actNotify({
            message: "Đã có lỗi xảy ra vui lòng thử lại sau!",
            color: "danger"
          })
        );
      });
  };
};

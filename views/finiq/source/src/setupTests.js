import { configure } from "enzyme";
import utils from "jsdom/lib/jsdom/utils";
import Adapter from "enzyme-adapter-react-16";
import canvasMock from "canvas-mock";

function Canvas() {
  canvasMock(this);
  this.toDataURL = function() {
    return "";
  };
}
utils.Canvas = Canvas;

configure({ adapter: new Adapter() });
